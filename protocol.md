# HTTP/2.0

## Why?

Performance problems:

- Only 1 request per TCP connection (HTTP/1.0); pipelining solves this problem partially and still suffers from head-of-line blocking – concurrency is actually achieved by opening multiple connections.
- HTTP headers are verbose and repetitive

HTTP/2 allows interleaving of requests and responses on the same TCP connection, prioritization of requests, efficient coding for HTTP header fields, and efficient message processing using binary message framing.

## Overview

The basic protocol unit of HTTP/2 is a frame of a given type (for example, HEADERS, DATA, SETTINGS, WINDOW_UPDATE, PUSH_PROMISE).

Multiplexing is achieved by associating request/response exchanges with streams; streams are interleaved with other streams. A stalled stream does not block the others.

Push (new interaction mode) allows a server to push data to a client. This is a trade-off: the server anticipates the client needs to improve latency. It makes up a synthetic request and sends it to the server as a PUSH_PROMISE frame, then sends a response for that request on a separate stream.

HTTP header frames are compressed.

Terms:

- client: endpoint initiating HTTP/2 connections
- connection: transport-layer connection between two endpoints
- connection error: error that affects the entire HTTP/2 connection
- endpoint: client or server
- frame: smallest unit of communication within an HTTP/2 connection, comprising a header and variable-length sequence of octets structured according to the frame type
- peer: endpoint (the opposing endpoint relative to the subject)
- receiver: endpoint receiving frames
- sender: endpoint transmitting frames
- server: endpoint that did not initiate the connection
- stream: bi-directional flow of frames across a virtual channel within the HTTP/2 connection
- stream error: error that affects a particular stream within an HTTP/2 connection

## Starting HTTP/2

URIs are unchanged.

### Version

`h2` for HTTP/2 over TLS (ALPN: 0x68, 0x32), `h2c` for clear-text.


