package api4

import concurrent.Future
import scalaz.\/
import shapeless._
import shapeless.ops.hlist._

trait Request
trait Response

trait Documentable {
  def name: String = ???
  def doc: String = ???
}

trait FailedExtraction

trait Extractor[I, O <: HList] extends Documentable {
  def apply(input: I): FailedExtraction \/ (O, I)
}
trait ExtractorBuilder[I, E[O <: HList] <: Extractor[I, O] with Extractor[I, _ <: HList]] {
  def build[O <: HList](name: String, doc: String)(extract: I => FailedExtraction \/ (O, I)): E[O]
}
object ExtractorBuilder {
  type FIE[XI, XE[O <: HList] <: Extractor[XI, O] with Extractor[XI, O]] = {
    type I = XI
    type E[O <: HList] = XE[O]
  }
}

object Extractor {
  type Checker[I] = Extractor[I, HNil]

  implicit class ExtendedExtract2[I, OA <: HList, E[O <: HList] <: Extractor[I, O]](val ea: E[OA] with Extractor[I, OA]) extends AnyVal {
    def ~[OB <: HList](eb: E[OB])(implicit m: Prepend[OA, OB], builder: ExtractorBuilder[I, E]): E[m.Out] =
      builder.build(ea.name, ea.doc) { ia =>
        ea(ia).flatMap { case (a, ib) =>
          eb(ib).map { case (b, ic) => (a ::: b, ic) }
        }
      }
  }
}

object api4 {
  type Headers = Map[String, String]
  case class PrematureResponse[R <: Response](response: R) extends FailedExtraction
  trait HeaderExtractor[O <: HList] extends Extractor[Headers, O]
  type HE[I, O <: HList] = HeaderExtractor[O]
  object HeaderExtractor {
    implicit val builder = new ExtractorBuilder[Headers, HeaderExtractor] {
      def build[O <: HList](name: String, doc: String)(extract: Headers => FailedExtraction \/ (O, Headers)): HeaderExtractor[O] = ???
    }
  }

  import HeaderExtractor._

  val im = implicitly[Prepend[String :: HNil, Int :: HNil]]

  val h1: HeaderExtractor[String :: HNil] = ???
  val h2: HeaderExtractor[Int :: HNil] = ???

  new Extractor.ExtendedExtract2[Headers, String :: HNil, HeaderExtractor](h1)

  new Extractor.ExtendedExtract2(h1)
  import Extractor._

  h1.~(h2)//(im, builder)
}

trait PathExtractor[A] {
  def extract(s: String): FailedExtraction \/ (A, String) = ???
}
object PathExtractor {
  import shapeless.ops.function._
  implicit class ExtendedPathExtractor2[A <: HList](val ea: PathExtractor[A]) extends AnyVal {
    def ~[B](eb: PathExtractor[B])(implicit m: Prepend[A, B :: HNil]): PathExtractor[m.Out] = new PathExtractor[m.Out] {
      override def extract(s: String) = ea.extract(s).flatMap { case (a, s) =>
        eb.extract(s).map { case (b, s) => (a ::: (b :: HNil), s) }
      }
    }
    def ~[B <: HList](eb: PathExtractor[B])(implicit m: Prepend[A, B], ev: B <:< HList): PathExtractor[m.Out] = new PathExtractor[m.Out] {
      override def extract(s: String) = ea.extract(s).flatMap { case (a, s) =>
        eb.extract(s).map { case (b, s) => (a ::: b, s) }
      }
    }
  }
  implicit class ExtendedPathExtractor[A](val ea: PathExtractor[A]) extends AnyVal {
    def ~[B](eb: PathExtractor[B]): PathExtractor[A :: B :: HNil] = new PathExtractor[A :: B :: HNil] {
      override def extract(s: String) = ea.extract(s).flatMap { case (a, s) =>
        eb.extract(s).map { case (b, s) => (a :: b :: HNil, s) }
      }
    }
    def ~[B <: HList](eb: PathExtractor[B])(implicit ev: B <:< HList): PathExtractor[A :: B] = new PathExtractor[A :: B] {
      override def extract(s: String) = ea.extract(s).flatMap { case (a, s) =>
        eb.extract(s).map { case (b, s) => (a +: b, s) }
      }
    }
  }
}

trait HeaderExtractor[A, Response]
trait BodyParser[A]

trait RequestBuilder {
  //def withParsers[H <: HList](parsers: H)(implicit ev: LUBConstraint[H, BodyParser[_]])
  def accept[A](bp: BodyParser[A])(f: A => Future[Response])
}

trait HeaderFormat
trait ResponseFormat




object test {
  class Person

  val p: PathExtractor[String] = ???
  val p1: PathExtractor[Person] = ???
  val p2: PathExtractor[Int] = ???

  val p3: PathExtractor[String :: Int :: HNil] = p ~ p2

  import shapeless.syntax.std.function._

  val hl = "truc" :: 2 :: HNil

  def plop(hl: String :: Int :: HNil): String = ""

  //p3.app("", 4)

  val gg = (plop _).fromProduct
  //gg :String

  val p4: PathExtractor[String :: String :: Int :: HNil] = p ~ p3
  val p5: PathExtractor[String :: Int :: String :: String :: Int :: HNil] = p3 ~ p4
  val p6: PathExtractor[String :: Int :: Person :: HNil] = p3 ~ p1
}



