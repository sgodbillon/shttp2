package shttp2

import concurrent.{ ExecutionContext, Future }

trait Truc
trait S2[A, B]
trait S[M[_]]

object toto {
  type S2A[A] = ({
    type Curried[B] = S2[A, B]
  })

  def l: S[S2A[String]#Curried] = ???

  type Curry[M[A, B], A2] = ({
    //type With[A] = ({
      type Curried[B] = M[A2, B]
    //})
  })

  def l2: S[Curry[S2, String]#Curried] = ???
}



trait OutStream
trait InStream
trait TransferResult

trait Response
trait Request

trait Push extends OutStream {
  def request: Request
  def response: Future[Response]
}

trait HttpConnection {
  def <<(p: Push): Future[TransferResult]
}

object test {
  def push: Push = ???
  def conn: HttpConnection = ???

  conn << push
}