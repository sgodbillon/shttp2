package api2

trait Request
trait Response

sealed trait Validation[A, +B]
trait Accepted[A, +B] extends Validation[A, B] { def get: B }
trait Refused[A, +B] extends Validation[A, B]

trait Validator[A, +B] {
  def validate(a: A): Validation[A, B]
}

trait PathMatcher {
  def matches: List[Any] // TODO
}

sealed trait ResourceMethod extends Validator[Request, Response] {
  def name: String
}

object ResourceMethod {
  trait Get extends ResourceMethod { final def name = "GET" }
  trait Post extends ResourceMethod { final def name = "POST" }
  trait Put extends ResourceMethod { final def name = "PUT" }
  trait Delete extends ResourceMethod { final def name = "DELETE" }
  trait Head extends ResourceMethod { final def name = "HEAD" }
  trait Options extends ResourceMethod { final def name = "OPTIONS" }
  trait Patch extends ResourceMethod { final def name = "PATCH" }
}

trait Resource {
  def matcher: PathMatcher

  def methods: List[ResourceMethod]
}


