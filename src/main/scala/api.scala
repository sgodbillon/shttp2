package api1

package http {
  sealed trait StatusCode {
    def code: Int
    def message: String
    def render: String = s"$code $message"
  }
  object StatusCode {
    object Ok extends StatusCode { val code = 200; val message = "OK" }
    object BadRequest extends StatusCode { val code = 400; val message = "BAD REQUEST" }
    object Forbidden extends StatusCode { val code = 403; val message = "FORBIDDEN" }
    object NotFound extends StatusCode { val code = 404; val message = "NOT FOUND" }
    object InternalServerError extends StatusCode { val code = 500; val message = "INTERNAL SERVER ERROR" }
  }
  sealed trait Method {
    def name: String
  }
  object Method {
    object Get extends Method { val name = "GET" }
    object Post extends Method { val name = "POST" }
    object Put extends Method { val name = "PUT" }
    object Delete extends Method { val name = "DELETE" }
    object Patch extends Method { val name = "PATCH" }
    object Head extends Method { val name = "HEAD" }
    object Options extends Method { val name = "OPTIONS" }
    object Trace extends Method { val name = "TRACE" }
    object Connect extends Method { val name = "CONNECT" }
  }
}

trait ContentType
object ContentType {
  object Html extends ContentType
  object Json extends ContentType
  object Xml extends ContentType
}

trait Req {
  def path: String
  def rawQueryString: Seq[(String, String)]
  def rawHeaders: Seq[(String, String)]
}

trait RejectedReq

trait Transform[R1 <: Req, R2 <: Req] {
  def apply(r: R1): Either[RejectedReq, R2]
}

trait Content[+C <: ContentType, X]

trait Request {
  type AcceptedContentType <: ContentType

  def body[X](implicit c: Content[AcceptedContentType, X]): X = ???
}
trait Response

trait Route { self =>
  type AcceptedContentType <: ContentType

  def path: String
  def proceed(req: Request { type AcceptedContentType = self.AcceptedContentType }): Response
}

object test {
  trait JsValue
  implicit object JsValueCT extends Content[ContentType.Html.type, JsValue]
  new Route {
    type AcceptedContentType = ContentType.Html.type
    val path = ""
    def proceed(req: Request { type AcceptedContentType = ContentType.Html.type }) = {
      req.body[JsValue]
      ???
    }
  }
}
