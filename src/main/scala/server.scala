package shttp2

import io.netty.bootstrap.ServerBootstrap
import io.netty.buffer.{ ByteBuf, Unpooled }
import io.netty.channel.{
  ChannelFuture,
  ChannelFutureListener,
  ChannelHandler,
  ChannelHandlerAdapter,
  ChannelHandlerContext,
  ChannelInitializer,
  ChannelOption,
  EventLoopGroup,
  SimpleChannelInboundHandler }
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.AsciiString
import io.netty.handler.codec.http.{
  DefaultFullHttpResponse,
  FullHttpResponse,
  HttpHeaderUtil,
  HttpHeaderValues,
  HttpRequest,
  HttpResponseStatus,
  HttpServerCodec,
  HttpServerUpgradeHandler }
import io.netty.handler.codec.http.HttpHeaderNames._
import io.netty.handler.codec.http.HttpResponseStatus._
import io.netty.handler.codec.http.HttpVersion._
import io.netty.handler.codec.http2._
import io.netty.handler.logging.{ LogLevel, LoggingHandler }
import io.netty.handler.ssl.{
  ApplicationProtocolConfig,
  SslContext,
  SslProvider,
  SupportedCipherSuiteFilter }
import io.netty.handler.ssl.ApplicationProtocolConfig.{ Protocol, SelectedListenerFailureBehavior, SelectorFailureBehavior }
import io.netty.handler.ssl.util.SelfSignedCertificate
import io.netty.util.CharsetUtil
import io.netty.util.internal.logging.{ InternalLogLevel, InternalLoggerFactory }
import java.nio.charset.Charset
import java.util.Collections
import javax.net.ssl.SSLEngine


object HelloWorldHttp2Handler {
  val UpgradeResponseHeader = "Http-To-Http2-Upgrade"

  private val FrameLogger = new Http2FrameLogger(InternalLogLevel.INFO, InternalLoggerFactory.getInstance(classOf[HelloWorldHttp2Handler]))
}

object FrameListener {
  val ResponseBytes = Unpooled.unreleasableBuffer(Unpooled.copiedBuffer("Hello World", CharsetUtil.UTF_8));
}

class FrameListener extends Http2FrameAdapter {
  // ew!
  private var encoder: Http2ConnectionEncoder = _

  def useEncoder(encoder: Http2ConnectionEncoder): this.type = {
    this.encoder = encoder
    this
  }

  import FrameListener._

  /**
   * If receive a frame with end-of-stream set, send a pre-canned response.
   */
  override def onDataRead(ctx: ChannelHandlerContext, streamId: Int, data: ByteBuf, padding: Int, endOfStream: Boolean): Int = {
    println("onDataRead: " + data.toString(Charset.forName("UTF-8")))
    val processed = data.readableBytes() + padding
    if (endOfStream) {
      sendResponse(ctx, streamId, ResponseBytes.duplicate())
    }
    processed
  }

  /**
   * If receive a frame with end-of-stream set, send a pre-canned response.
   */
  override def onHeadersRead(ctx: ChannelHandlerContext, streamId: Int, headers: Http2Headers, streamDependency: Int, weight: Short, exclusive: Boolean, padding: Int, endStream: Boolean): Unit = {
    println(s"onHeadersRead")
    if (endStream)
      sendResponse(ctx, streamId, ResponseBytes.duplicate())
  }

  /**
   * Sends a "Hello World" DATA frame to the client.
   */
  private def sendResponse(ctx: ChannelHandlerContext, streamId: Int, payload: ByteBuf): Unit = {
    // Send a frame for the response status
    println("sending on $streamId: " + payload.toString(Charset.forName("UTF-8")))
    val headers = new DefaultHttp2Headers().status(HttpResponseStatus.OK.codeAsText())
    encoder.writeHeaders(ctx, streamId, headers, 0, false, ctx.newPromise())
    encoder.writeData(ctx, streamId, payload, 0, true, ctx.newPromise())
    ctx.flush()
  }
}

class HelloWorldHttp2Handler(
  connection: Http2Connection,
  frameReader: Http2FrameReader,
  frameWriter: Http2FrameWriter,
  listener: FrameListener) extends Http2ConnectionHandler(connection, frameReader, frameWriter, listener) { self =>

  import HelloWorldHttp2Handler._

  def this() = this(
    new DefaultHttp2Connection(true),
    new Http2InboundFrameLogger(new DefaultHttp2FrameReader(), HelloWorldHttp2Handler.FrameLogger),
    new Http2OutboundFrameLogger(new DefaultHttp2FrameWriter(), HelloWorldHttp2Handler.FrameLogger),
    new FrameListener
  )

  listener.useEncoder(encoder())

  /**
   * Handles the cleartext HTTP upgrade event. If an upgrade occurred, sends a simple response via HTTP/2
   * on stream 1 (the stream specifically reserved for cleartext HTTP upgrade).
   */
  override def userEventTriggered(ctx: ChannelHandlerContext, evt: Object): Unit = {
    evt match {
      case evt: HttpServerUpgradeHandler.UpgradeEvent =>
        // Write an HTTP/2 response to the upgrade request
        val headers =
          new DefaultHttp2Headers().status(HttpResponseStatus.OK.codeAsText()).
            set(new AsciiString(UpgradeResponseHeader), new AsciiString("true"))
        encoder().writeHeaders(ctx, 1, headers, 0, true, ctx.newPromise())
      case _ => super.userEventTriggered(ctx, evt)
    }
  }

  override def onException(ctx: ChannelHandlerContext, cause: Throwable): Unit = {
    println(s"onException from server")
    cause.printStackTrace()
    super.onException(ctx, cause)
  }

  override def exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable): Unit = {
    cause.printStackTrace()
    ctx.close()
  }

  private class SimpleHttp2FrameListener() extends FrameListener {
    val encoder = self.encoder()
  }
}

class Http2ServerInitializer(sslContext: Option[SslContext]) extends ChannelInitializer[SocketChannel] {
  private def configureClearText(ch: SocketChannel): ch.type = {
    val sourceCodec = new HttpServerCodec()
    val upgradeCodec = new Http2ServerUpgradeCodec(new HelloWorldHttp2Handler())
    val upgradeHandler = new HttpServerUpgradeHandler(sourceCodec, Collections.singletonList(upgradeCodec), 65536);

    ch.pipeline().addLast(sourceCodec)
    ch.pipeline().addLast(upgradeHandler)
    ch.pipeline().addLast(new UserEventLogger())
    ch
  }

  def initChannel(ch: SocketChannel): Unit = sslContext match {
    case Some(ctx) => configureSsl(ch, ctx)
    case None => configureClearText(ch)
  }

  /**
   * Configure the pipeline for TLS NPN negotiation to HTTP/2.
   */
  private def configureSsl(ch: SocketChannel, ctx: SslContext): Unit = {
    ch.pipeline().addLast(ctx.newHandler(ch.alloc()), new Http2OrHttpHandler())
  }
}

class Http2OrHttpHandler(maxHttpContentLength: Int = Http2OrHttpHandler.MaxContentLength) extends Http2OrHttpChooser(maxHttpContentLength) {

  override protected def getProtocol(engine: SSLEngine): Http2OrHttpChooser.SelectedProtocol = {
    val protocol = engine.getSession().getProtocol().split(":")
    println("Sc. Array(" + protocol.mkString(", ") + ")")
    if (protocol != null && protocol.length > 1) {
      val selectedProtocol = Http2OrHttpChooser.SelectedProtocol.protocol(protocol(1))
      println(s"Sc. Selected Protocol is $selectedProtocol")
      selectedProtocol
    } else Http2OrHttpChooser.SelectedProtocol.UNKNOWN
  }


  override protected def createHttp1RequestHandler(): ChannelHandler =
    new HelloWorldHttp1Handler()

  override protected def createHttp2RequestHandler(): Http2ConnectionHandler =
    new HelloWorldHttp2Handler()
}

object Http2OrHttpHandler {
  private val MaxContentLength = 1024 * 100
}

class HelloWorldHttp1Handler extends SimpleChannelInboundHandler[HttpRequest] {

  override def messageReceived(ctx: ChannelHandlerContext, req: HttpRequest): Unit = {
    println("messageReceived HTTP_1_1")
    if (HttpHeaderUtil.is100ContinueExpected(req)) {
      ctx.write(new DefaultFullHttpResponse(HTTP_1_1, CONTINUE))
    }
    val keepAlive = HttpHeaderUtil.isKeepAlive(req)

    val content = ctx.alloc().buffer()
    content.writeBytes(FrameListener.ResponseBytes.duplicate())

    val response = new DefaultFullHttpResponse(HTTP_1_1, OK, content)
    response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8")
    response.headers().setInt(CONTENT_LENGTH, response.content().readableBytes())

    if (!keepAlive) {
      ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE)
    } else {
      response.headers().set(CONNECTION, HttpHeaderValues.KEEP_ALIVE)
      ctx.writeAndFlush(response)
    }
  }

  override def exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable): Unit = {
    cause.printStackTrace()
    ctx.close()
  }
}


class UserEventLogger() extends ChannelHandlerAdapter {
  override def userEventTriggered(ctx: ChannelHandlerContext, evt: Any): Unit = {
    println(s"User Event Triggered: $evt")
    ctx.fireUserEventTriggered(evt)
  }
}

class HttpServer(val ssl: Boolean = false) {
  val port = if(ssl) 8443 else 8080

  def run(): Unit = {
    val sslCtx =
      if (ssl) {
        val ssc = new SelfSignedCertificate()
        Some(
          SslContext.newServerContext(
            SslProvider.JDK,
            ssc.certificate(),
            ssc.privateKey(),
            null,
            Http2SecurityUtil.CIPHERS,
            /* NOTE: the following filter may not include all ciphers required by the HTTP/2 specification
             * Please refer to the HTTP/2 specification for cipher requirements. */
            SupportedCipherSuiteFilter.INSTANCE,
            new ApplicationProtocolConfig(
              Protocol.ALPN,
              SelectorFailureBehavior.FATAL_ALERT,
              SelectedListenerFailureBehavior.FATAL_ALERT,
              Http2OrHttpChooser.SelectedProtocol.HTTP_2.protocolName(),
              Http2OrHttpChooser.SelectedProtocol.HTTP_1_1.protocolName()),
            0, 0))
      } else None
    // Configure the server.
    val bossGroup = new NioEventLoopGroup(1)
    val workerGroup = new NioEventLoopGroup()
    try {
      val b =
        new ServerBootstrap().
          option(ChannelOption.SO_BACKLOG.asInstanceOf[ChannelOption[Any]], 1024).
          group(bossGroup, workerGroup).
          channel(classOf[NioServerSocketChannel]).
          handler(new LoggingHandler(LogLevel.ERROR)).
          childHandler(new Http2ServerInitializer(sslCtx))

      val ch = b.bind(port).sync().channel()

      System.err.println("(Scala) Open your HTTP/2-enabled web browser and navigate to " +
              (if(ssl) "https" else "http") + "://127.0.0.1:" + port + '/')

      ch.closeFuture().sync()
    } finally {
      bossGroup.shutdownGracefully()
      workerGroup.shutdownGracefully()
    }
  }
}

object Test extends App {
  new HttpServer(true).run()
}