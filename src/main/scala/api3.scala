package api3

import shapeless._

trait Request; trait Response
object Response {
  object DefaultBadRequest extends Response
}
trait Route

sealed trait Validation
trait Extractor[+A, +R <: Response] extends Validation {
  def extract(request: Request): A = ???
}
object Extractor {
  type DefaultExtractor[A] = Extractor[A, Response.DefaultBadRequest.type]
}
//import Extractor.DefaultExtractor
trait DefaultExtractor[A] extends Extractor[A, Response.DefaultBadRequest.type]

trait Processor[Extracts <: HList, Failures <: HList] {
  def ~[A, R <: Response](extractor: Extractor[A, R]): Processor[A :: Extracts, R :: Failures]
  def extract(f: Extracts => Response): Route
}

object Processor {
  implicit def extractorAsProcessor[A, R <: Response](extractor: Extractor[A, R]): Processor[A :: HNil, R :: HNil] = ???
  /*implicit class ExtractorAsProcessor[A, R <: Response](val extractor: Extractor[A, R]) extends AnyVal {

  }*/
}

sealed trait Verb {
  //def process[Extracts <: HList, Failures <: HList, P <: Processor[Extracts, Failures]](p: P)(f: Extracts => Response): Route = ???
  def process[H <: HList](h: H)(implicit ev: LUBConstraint[H, Extractor[_, Response]]) = ???
}
object Verb {
  object Get extends Verb
}

object test {
  val e1: DefaultExtractor[Int] = ???
  val e2: DefaultExtractor[Float] = ???

  type EE = DefaultExtractor[Int] :: DefaultExtractor[Float] :: HNil

  val ee: EE = e1 :: e2 :: HNil

  /*Verb.Get.process(e1 :: HNil)(
    BoundedHList.boundedHList[HNil, DefaultExtractor[Int], Extractor[_, Response]](ee1)(
      BoundedHList.boundedHNil(HNil), check0(e1)))*/


  Verb.Get.process(ee)//(BoundedHList.boundedHNil(HNil))
  /*Verb.Get.process (ee) { hl =>
    ???
  }*/
}