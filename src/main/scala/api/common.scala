package api


trait Headers
trait Path

trait Request {
  def headers: Headers
  def path: Path
}
trait Response
object Response {
  object DefaultBadRequest extends Response
}

trait Documentable {
  def name: String
  def description: String
}

sealed trait StatusCode {
    def code: Int
    def message: String
    def render: String = s"$code $message"
  }
object StatusCode {
  object Ok extends StatusCode { val code = 200; val message = "OK" }
  object BadRequest extends StatusCode { val code = 400; val message = "BAD REQUEST" }
  object Forbidden extends StatusCode { val code = 403; val message = "FORBIDDEN" }
  object NotFound extends StatusCode { val code = 404; val message = "NOT FOUND" }
  object InternalServerError extends StatusCode { val code = 500; val message = "INTERNAL SERVER ERROR" }
}
sealed trait Method {
  def name: String
}
object Method {
  object Get extends Method { val name = "GET" }
  object Post extends Method { val name = "POST" }
  object Put extends Method { val name = "PUT" }
  object Delete extends Method { val name = "DELETE" }
  object Patch extends Method { val name = "PATCH" }
  object Head extends Method { val name = "HEAD" }
  object Options extends Method { val name = "OPTIONS" }
  object Trace extends Method { val name = "TRACE" }
  object Connect extends Method { val name = "CONNECT" }
}