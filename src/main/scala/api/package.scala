import shapeless._

import api.extractors._

package object api {
  type PathExtractor[F <: Coproduct, O <: HList] = Extractor[Path, F, O]
  //type PathDefinition[A <: HList]
}