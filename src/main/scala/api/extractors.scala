package api.extractors

import concurrent.Future
import shapeless._
import shapeless.ops.coproduct._
import shapeless.ops.hlist._

import api.Documentable

trait Extractor[I, F <: Coproduct, O <: HList] extends Documentable {
  def extract(input: I): (O :+: F, I)
}

object Extractor {

  implicit class ExtractorOps[I, FA <: Coproduct, OA <: HList, E <: Extractor[I, FA, OA]](val ea: E with Extractor[I, FA, OA]) extends AnyVal {
    private type λ[F <: Coproduct, O <: HList] = Extractor[I, F, O]

    def ~[FB <: Coproduct, OB <: HList](eb: λ[FB, OB])(implicit hp: Prepend[OA, OB], ex: ExtendBy[FA, FB]): λ[ex.Out, hp.Out] =
      flatMap { oa =>
        eb.map { ob =>
          oa ::: ob
        }
      }

    def and[FB <: Coproduct, OB <: HList](eb: λ[FB, OB])(implicit hp: Prepend[OA, OB], ex: ExtendBy[FA, FB]): λ[ex.Out, hp.Out] = ea ~ eb

    def map[FB <: Coproduct, OB <: HList](f: OA => OB): λ[FA, OB] = new Extractor[I, FA, OB] {
      def name = ea.name // TODO
      def description = ea.description
      def extract(input: I): (OB :+: FA, I) = {
        val (cp, i) = ea.extract(input)
        val mapped = (cp.head, cp.tail) match {
          case (Some(oa), None) => Coproduct[OB :+: FA](f(oa))
          case (None, Some(fa)) => Inr[OB, FA](fa)
          case _ => sys.error("should not happen")
        }
        mapped -> i
      }
    }

    def flatMap[FB <: Coproduct, OB <: HList](f: OA => Extractor[I, FB, OB])(implicit ex: ExtendBy[FA, FB]): λ[ex.Out, OB] = new Extractor[I, ex.Out, OB] {
      def name = ea.name // TODO
      def description = ea.description
      def extract(input: I): (OB :+: ex.Out, I) = {
        val (ra, i) = ea.extract(input)
        val result = ra.head match {
          case Some(oa) =>
            val (rb, i2) = f(oa).extract(i)
            rb.head match {
              case Some(ob) => Inl[OB, ex.Out](ob)
              case _ => Inr[OB, ex.Out](ex.left(rb.tail.get))
            }
          case _ => Inr[OB, ex.Out](ex.right(ra.tail.get))
        }
        result -> i
      }
    }

    def map2[FB <: Coproduct, OB <: HList](f: OA :+: FA => OB :+: FB): λ[FB, OB] = new Extractor[I, FB, OB] {
      def name = ea.name // TODO
      def description = ea.description
      def extract(input: I): (OB :+: FB, I) = {
        val (cp, i) = ea.extract(input)
        f(cp) -> i
      }
    }

    def flatMap2[FB <: Coproduct, OB <: HList](f: (OA :+: FA) => Extractor[I, FB, OB]): λ[FB, OB] = new Extractor[I, FB, OB] {
      def name = ea.name // TODO
      def description = ea.description
      def extract(input: I): (OB :+: FB, I) = {
        val (cp, i) = ea.extract(input)
        f(cp).extract(i)
      }
    }

  }

  def unary[I, F, O](name: String, description: String)(f: I => (Either[F, O], I)): Extractor[I, F :+: CNil, O :: HNil] = ???

  def apply[I, F <: Coproduct, O <: HList](nme: String, desc: String)(f: I => (O :+: F, I)): Extractor[I, F, O] = new Extractor[I, F, O] {
    def name = nme
    def description = desc

    def extract(input: I): (O :+: F, I) = f(input)
  }
}

object test {
  type Bidule = Double :+: Boolean :+: CNil
  type Plop = String :+: Int :+: CNil
  val plop = Coproduct[Plop](0)
  val c = Coproduct[Plop :+: Bidule :+: Long :+: CNil](plop)


  def coucou[L <: Coproduct, R <: Coproduct](l: L, r: R)(implicit ex: ExtendBy[L, R]): ex.Out = {
    ex.right(l)
    ???
  }

  trait HHH[A]
  trait JJJ[A]

  /*implicit class ppp(val m: Machin) extends AnyVal {
    def get[A](name: String)(implicit ev: HHH[A]): Unit = {}
  }

  implicit class ppp2(val m: Machin) extends AnyVal {
    def get[A](name: String)(implicit ev: JJJ[A]): Unit = {}
  }

  trait Truc[A] {
    def apply(implicit ev: HHH[A]): Unit = ???
    def apply(implicit ev: JJJ[A]): Unit = ???
  }*/

  class Machin {
    type M = String
    def get[A](name: String)(implicit ev: HHH[A]): Unit = {}
    def get[A](name: M)(implicit ev: JJJ[A], k: A <:< AnyRef): Unit = {}
    /*object get { def apply[A](name: String)(implicit ev: HHH[A]): Unit = {} }

    def get[A](name: String)(implicit ev: JJJ[A]): Unit = {}*/
  }

  val m: Machin = ???

  implicit val jjj: HHH[String] = ???

  m.get[String]("")
}

/*
object Extractor {
  implicit class ExtractorOps[I, FA <: Coproduct, OA <: HList, E[F <: Coproduct, O <: HList] <: Extractor[I, F, O]](val ea: E[FA, OA] with Extractor[I, FA, OA]) extends AnyVal {
    def ~[FB <: Coproduct, OB <: HList](eb: E[FB, OB])(implicit m: Prepend[OA, OB], builder: ExtractorBuilder[I, FB, E]): E[FB, m.Out] =
      builder.build(ea.name, ea.description) { ia =>
        ea.extract(ia).flatMap { case (a, ib) =>
          eb.extract(ib).map { case (b, ic) => (a ::: b, ic) }
        }
      }
  }
}

trait ExtractorBuilder[I, E[F <: Coproduct, O <: HList] <: Extractor[I, F, O]] {
  def build[F <: Coproduct, O <: HList](name: String, description: String)(extractor: I => F \/ (O, I)): E[O]
}*/