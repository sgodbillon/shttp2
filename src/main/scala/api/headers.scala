package api.headers

import shapeless._
import api._
import api.extractors._

//import api.extractors.Extractor._
package object extractors {
  trait HeaderExtractor[F <: Coproduct, O <: HList] extends Extractor[Headers, F, O]
  type UnaryHeaderExtractor[F <: Response, O] = HeaderExtractor[F :+: CNil, O :: HNil]

  object HeaderExtractor {
    def apply[A, R <: Response](name: String, description: String)(f: Headers => (Either[R, A], Headers)): UnaryHeaderExtractor[R, A] = ???
  }

  object ContentTypeExtractor extends UnaryHeaderExtractor[Response.DefaultBadRequest.type, String] {
    def name = "RequiredContentType"
    def description = "The ContentType header is required"

    def extract(headers: Headers): ( (String :: HNil) :+: Response.DefaultBadRequest.type :+: CNil, Headers) = ???
  }
  //val coucou: String = Extractor.plop(ContentTypeExtractor)

  val plop = ContentTypeExtractor ~ ContentTypeExtractor

  case class FailedExtraction[R <: Response](
    name: String,
    description: String,
    response: Response
  ) extends Documentable
}