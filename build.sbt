name := "shttp2"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.11.5"

//scalacOptions += "-Xlog-implicits"

resolvers += Resolver.mavenLocal

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

resolvers += "Typesafe Repository Snapshots" at "http://repo.typesafe.com/typesafe/snapshots/"

libraryDependencies ++= Seq(
  "io.netty" % "netty-codec-http2" % "5.0.0.Alpha2-SNAPSHOT",
  "org.mortbay.jetty.alpn" % "alpn-boot" % "8.1.2.v20141202",
  "com.chuusai" %% "shapeless" % "2.1.0",
  "org.scalaz" %% "scalaz-core" % "7.1.1"
)

javaOptions in run += "-Xbootclasspath/p:/Volumes/Data/code/shttp2/alpn-boot-8.1.2.v20141202.jar"

fork in run := true